<!DOCTYPE html>
<?php wp_head(); ?>

<body>

<h1><?php bloginfo('sitename'); ?></h1>

<?php 
wp_nav_menu(
    ['theme_location' => 'main_menu']
); 
?>
<h2><?php bloginfo('description'); ?></h2>