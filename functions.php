<?php

show_admin_bar(true);

// Zone de menus
add_action('init', function(){
    register_nav_menus([
        'main_menu' => 'Menu principal',
        'footer_menu' => 'Menu du pied'
    ]);
});

// Zone de widgets
add_action('widgets_init', function(){
    register_sidebar([
        'id' => "footer_widgets",
        'name'=> 'Zone du pied de page',
        'description' => "Ces widgets vont dans le pied de page"
    ]);
});

// Créer une zone de widgets
include 'CustomWidget.php';