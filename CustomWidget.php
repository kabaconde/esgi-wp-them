<?php

class CustomWidget extends WP_Widget
{

    function __construct()
    {
		// Instantiate the parent object
        $options = [
            'classname' => 'custom_widget',
            'description' => 'Widget lien facile'
        ];
        //$this->WP_widget("custom_widget", "Custom Link Widget", $options);
        parent::__construct(false, 'Custom Link Widget');
    }

    function widget($args, $instance)
    {
        // Widget output
        echo '<a href="' . $instance['url'] . '">' . $instance['name'] . '</a>';
    }
    
    function update($new_instance, $old_instance)
    {
        return $new_instance;
    }
    
    function form($instance)
    {
        // Output admin widget options form
        $defaut = [
            "name" => "Google",
            "url" => "http://www.google.com"
        ];
        
        $instance = wp_parse_args($instance, $defaut);

        echo '
            <p>
                <label for="'. $this->get_field_id('name') .'">Name: </label>
                <input value="'. esc_attr($instance['name']). '" name="'. $this->get_field_name('name').'" id="'. $this->get_field_id('name'). '" type="text"/>
            </p>
            <p>
                <label for="'. $this->get_field_id('url'). '">URL: </label>
                <input value="'. esc_attr($instance['url']). '" name="'. $this->get_field_name('url'). '" id="'. $this->get_field_id('url'). '" type="text"/>
            </p>
        ';
    }
}

add_action('widgets_init', function () {
    register_widget('CustomWidget');
});